/* Array de Dados de Artistas
 *
 * name: Nome do Artista
 * role: Número da Categoria do Artista
 * image: Ficheiro de Imagem do Artista
 * biography: Biografia do Artista
 * material.name: Nome do Material do Artista
 * material.text: Descrição do Material do Artista
 * material.image: Ficheiro de Imagem do Material do Artista
 * brand.name: Nome da Banda do Artista
 * brand.date: Data de Participação do Artista na Banda
 * brand.image: Ficheiro de Imagem da Banda
 */
let artist = [
  {
    // 0: Anthony Kiedis
    name: "Anthony Kiedis",
    role: [2],
    image: "Anthony Kiedis.jpg",
    biography:
      "Anthony Kiedis é um cantor e compositor americano. É o vocalista, letrista e fundador da banda Red Hot Chili Peppers, banda que fundou em 1983 junto a seus amigos Flea, Hillel Slovak e Jack Irons. Kiedis escreveu em 2004 sua autobiografia, Scar Tissue. No livro, Anthony escreve sobre sua difícil vida, compartilha abertamente sobre o vício das drogas e conta sua dificuldade para pará-lo. Além disso, fala sobre sua amizade duradoura com Flea, sua infância e o início, onde tudo começou, o quão foi complicado colocar a banda, Red Hot, no topo.",
    material: [
      {
        name: "Audix OM7",
        text: "",
        image: "Audix OM7.JPG",
      },
    ],
    band: [
      {
        name: "Red Hot Chili Peppers",
        date: "",
        image: "Red Hot Chili Peppers.jpg",
      },
    ],
    highlight: true,
    views: 0,
    page: "artista_anthonykiedis.html",
  },
  {
    // 1: Chad Smith
    name: "Chad Smith",
    role: [1],
    image: "Chad Smith.jpg",
    biography:
      'Chad Gaylord Smith, ou como é mais conhecido, Chad Smith (Saint Paul, 25 de Outubro de 1961), é um baterista americano, mais conhecido como membro de longa data da banda californiana de rock Red Hot Chili Peppers, com quem se juntou em 1988 e permanece até hoje. Smith se juntou ao supergrupo Chickenfoot em 2008, quando os Chili Peppers estavam em hiato. Chad Smith também trabalhou com Glenn Hughes e também em seu proprio projeto de rock instrumental Chad Smith\'s Bombastic Meatbats. Além de bandas de Smith, ele também grava com muitos outros músicos, incluindo Dixie Chicks, Kid Rock e George Clinton. Smith ficou em #64 na lista dos "100 melhores bateristas de todos os tempos" pela Rolling Stone. Ele entrou para o Guinnes Book após tocar na maior bateria do mundo, constituída por 308 peças. Em 2012 foi introduzido ao Hall da Fama do Rock como membro do Red Hot Chili Peppers.',
    material: [
      {
        name: "Adams Professional Gen II Smooth Copper Timpani 32''",
        text: "",
        image: "Adams Professional Gen II Smooth Copper Timpani 32''.jpg",
      },
      {
        name: "Pearl 2000C Bass Pedal",
        text: "",
        image: "Pearl 2000C Bass Pedal.jpg",
      },
      {
        name: "Pearl SensiTone Elite 14 x 5''",
        text: "",
        image: "Pearl SensiTone Elite 14 x 5''.jpg",
      },
    ],
    band: [
      {
        name: "Red Hot Chili Peppers",
        date: "",
        image: "Red Hot Chili Peppers.jpg",
      },
    ],
    highlight: false,
    views: 0,
    page: "artista_chadsmith.html",
  },
  {
    // 2: Cory Henry
    name: "Cory Henry",
    role: [2, 6, 8],
    image: "Cory Henry.jpg",
    biography:
      'Cory Alexander Henry  (nascido em 27 de fevereiro de 1987) é um cantor e compositor, pianista, organista e produtor americano de R & B / Soul . Sendo um ex-membro da banda  “Snarky Puppy” , Cory lançou a sua carreira solo como artista com Art of Love, o seu primeiro lançamento independente. Cory foi selecionado por Quincy Jones para se integrar na sua série de "Soundtrack of America", abertura do The Shed em Nova Iorque. Cory agora faz tours e grava com a sua banda Cory Henry & The Funk Apostles.',
    material: [
      {
        name: "Hammond B3",
        text: "",
        image: "Hammond B3.jpg",
      },
      {
        name: "Moog Little Phatty Stage II",
        text: "",
        image: "Moog Little Phatty Stage II.jpg",
      },
      {
        name: "Moog Minimoog Voyager",
        text: "",
        image: "Moog Minimoog Voyager.jpg",
      },
    ],
    band: [
      {
        name: "Cory Henry & The Funk Apostles",
        date: "",
        image: "../no-image.jpg",
      },
    ],
    highlight: false,
    views: 0,
    page: "artista_coryhenry.html",
  },
  {
    // 3: Flea
    name: "Flea",
    role: [0, 9, 10],
    image: "Flea.jpg",
    biography:
      "Michael Peter Balzary (Melbourne, 16 de outubro de 1962), mais conhecido pelo seu nome artístico Flea, é um baixista australiano, considerado um dos maiores baixistas da história da música. Flea, que também toca  trompete e ocasionalmente trabalha como ator, é mais conhecido como baixista, co-fundador e um dos compositores da banda de rock alternativo Red Hot Chili Peppers. Flea também é o co-fundador da Silverlake Conservatory of Music, uma organização sem fins lucrativos de educação musical fundada em 2001. Originalmente um prodígio do trompete, Flea aprendeu a tocar baixo na escola secundária, com seu amigo íntimo (e futuro guitarrista do Red Hot Chili Peppers) Hillel Slovak, que precisava de um baixista para sua banda Anthym. Flea passou a integrar o grupo, porém abandonou-o alguns meses depois para tocar na banda de punk Fear. Logo juntou-se novamente a Slovak para formar uma banda destinada a apenas uma apresentação, com seus colegas de escola Anthony Kiedis e Jack Irons; a colaboração inesperada acabaria dando origem aos Red Hot Chili Peppers. Atualmente considerado como um dos melhores baixistas de todos os tempos, seu trabalho com a banda incorporou diversos estilos musicais, e abrangem, tecnicamente, desde o slap mais agressivo a métodos mais melódicos. Tem influências do funk e da música punk rock, seu estilo é primordialmente centrado na simplicidade e no minimalismo, não dispensa a complexidade como um recurso a ser utilizado quando necessário. Além dos Chili Peppers, Flea colaborou com diversos outros artistas, incluindo o Jane's Addiction, The Mars Volta e Alanis Morissette e mais recentemente, Rocket Juice and The Moon. Em abril de 2011, a revista Rolling Stone fez uma lista dos 10 melhores baixistas de todos os tempos, com Flea em segundo.",
    material: [
      {
        name: "EHX Q-Tron",
        text: "",
        image: "EHX Q-Tron.jpg",
      },
      {
        name: "Gallien Krueger 410RB",
        text: "",
        image: "Gallien Krueger 410RB.JPG",
      },
      {
        name: "Gallien Krueger 2001RB",
        text: "",
        image: "Gallien Krueger 2001RB.jpg",
      },
      {
        name: "MusicMan Stingray Special",
        text: "",
        image: "MusicMan Stingray Special.JPG",
      },
    ],
    band: [
      {
        name: "Red Hot Chili Peppers",
        date: "",
        image: "Red Hot Chili Peppers.jpg",
      },
      {
        name: "The Mars Volta",
        date: "",
        image: "../no-image.jpg",
      },
      {
        name: "Pigface",
        date: "",
        image: "Pigface.jpg",
      },
    ],
    highlight: false,
    views: 0,
    page: "artista_flea.html",
  },
  {
    // 4: Jacob Collier
    name: "Jacob Collier",
    role: [0, 1, 2, 5, 6, 8],
    image: "Jacob Collier.jpg",
    biography:
      'Jacob Collier (nascido em 2 de agosto de 1994) é multi-instrumentista inglês nascido em Londres, Inglaterra. Em 2012, as suas capas de vídeo em tela dividida de músicas populares, como " Don\'t You Worry \'s Thing ", de Stevie Wonder , começaram a tornar-se viral no YouTube . Em 2014, Collier assinou contrato com a empresa de administração de Quincy Jones e começou a trabalhar nas suas performancse ao vivo e audiovisuais. Em 2016, Collier lançou seu álbum de estreia, “In My Room” , que foi totalmente auto-gravado, mixado, apresentado e produzido na pequena sala dos fundos da casa da sua família em Finchley, norte de Londres. Em 2017 , Collier recebeu dois Grammys pelas suas músicas de " Flintstones " e " You And I ", ambos do álbum. Em 2018, Collier começou a trabalhar no Djesse , um álbum de quatro volumes e 50 músicas, com mais de duas dúzias de artistas e conjuntos. O primeiro álbum, que ele descreve como orquestral, Djesse vol. 1 , foi lançado em dezembro de 2018; o segundo, Djesse vol. 2, que ele diz de ser natureza mais acústica, foi lançado em julho de 2019. Em janeiro de 2020, Collier ganhou mais dois Grammy Awards pelas suas músicas de " All Night Long " no Djesse Vol. 1 e " Moon River ", no Djesse vol. 2 . Djesse vol. 3, que Collier diz ser mais baseado em espaço negativo e sons eletrônicos, concluiu a gravação em maio de 2020, com data de lançamento no verão de 2020.',
    material: [
      {
        name: "NS Design The CRT Double Bass",
        text: "",
        image: "NS Design The CRT Double Bass.jpg",
      },
      {
        name: "Solid State Logic L300",
        text: "",
        image: "Solid State Logic L300.jpg",
      },
      {
        name: "Yamaha U3",
        text: "",
        image: "Yamaha U3.jpg",
      },
    ],
    band: [],
    highlight: false,
    views: 0,
    page: "artista_jacobcollier.html",
  },
  {
    // 5: John Frusciante
    name: "John Frusciante",
    role: [5],
    image: "John Frusciante.jpg",
    biography:
      'John Anthony Frusciante  (Nova Iorque, 5 de março de 1970) é um guitarrista, cantor, compositor, pintor e produtor musical norte-americano. John é guitarrista da banda Red Hot Chili Peppers, com quem gravou 5 álbuns. Frusciante juntou-se aos Red Hot Chili Peppers aos 18 anos, entrando pela primeira vez no álbum Mother\'s Milk de 1989. O seguinte álbum, Blood Sugar Sex Magik foi um completo sucesso. John, então, deixou a banda por um longo período, retornando em 1998, para a gravação do álbum Californication. Em 15 de dezembro de 2009, o músico anunciou, mais uma vez, a sua saída do grupo. Em 15 de dezembro de 2019 a banda anunciou a volta do guitarrista para a banda. De momentos apenas se espera pelo seu próximo concerto. O seu trabalho é reconhecido pela revista americana Rolling Stone, que lhe concedeu o 18º lugar dos melhores guitarristas da história em 2003, o 40º lugar entre os "50 Melhores Guitarristas da História" segundo o site da fabricante de guitarras Gibson e também foi reconhecido como "O Melhor Guitarrista dos Últimos 30 Anos" (1980-2010) numa votação dos ouvintes, no site, da rádio britânica BBC. John Frusciante também mantem uma carreira solo, tendo lançado quinze álbuns sob o seu próprio nome. Além disso realizou duas colaborações com Josh Klinghoffer e Joe Lally, sob o nome de Ataxia. Os seus álbuns solo incorporam uma variedade de elementos desde o rock experimental, música ambiente até ao New Wave e música eletrônica. Retirando influência de guitarristas de vários gêneros, Frusciante enfatiza a melodia e a emoção quando toca guitarra favorecendo guitarras antigas e técnicas analógicas de gravação. Ele improvisava notas em seus shows pelo Red Hot Chili Peppers, dando tons diferentes aos solos. Em 14 de abril de 2012 foi induzido ao Rock and Roll Hall of Fame como membro do Red Hot Chili Peppers.',
    material: [
      {
        name: "Fender Stratocaster 1960",
        text: "",
        image: "Fender Stratocaster 1960.jpg",
      },
      {
        name: "Marshall 1960B 4x12'' Cabinet",
        text: "",
        image: "Marshall 1960B 4x12'' Cabinet.jpg",
      },
      {
        name: "Marshall JTM45 45W Tube Guitar Amp Head",
        text: "",
        image: "Marshall JTM45 45W Tube Guitar Amp Head.jpg",
      },
    ],
    band: [
      {
        name: "Red Hot Chili Peppers",
        date: "",
        image: "Red Hot Chili Peppers.jpg",
      },
      {
        name: "The Mars Volta",
        date: "",
        image: "../no-image.jpg",
      },
      {
        name: "Ataxia",
        date: "",
        image: "../no-image.jpg",
      },
    ],
    highlight: false,
    views: 0,
    page: "artista_johnfrusciante.html",
  },
  {
    // 6: Slash
    name: "Slash",
    role: [5],
    image: "Slash.jpg",
    biography:
      "Saul Hudson  (Londres, 23 de julho de 1965), conhecido pelo seu nome artístico Slash, é um guitarrista britânico-americano mundialmente famoso como integrante da formação clássica da banda “Guns N' Roses”, com quem alcançou sucesso mundial no final da década de 1980 e início dos anos 90. Na sua carreira posterior, Slash integrou algumas outras bandas de diversos estilos, bem-sucedidas na sua maioria, e em 2011 iniciou uma carreira solo lançando três discos e estando atualmente em sua terceira tour mundial. Slash completou a formação clássica do Guns N' Roses em 1986 e, no ano seguinte, o grupo lançou seu primeiro disco, Appetite for Destruction, que não teve nenhum grande impacto inicial, mas acabou ganhando grande popularidade com o tempo, transformando-se em um sucesso de vendas e consolidando a carreira do grupo. Em 1991, o conjunto lança seu novo disco dividido em duas partes, Use Your Illusion I e Use Your Illusion II, e apesar dos álbuns terem atingido grande sucesso, o relacionamento de Slash com o vocalista Axl Rose se deteriorou com o passar dos anos e o guitarrista deixou o grupo em 1996. Nos anos seguintes, Slash trabalhou com seu projeto independente, Slash's Snakepit, mas os problemas com drogas do músico e a péssima conduta dos demais integrantes levaram ao fim do grupo em 2001. Slash formou uma nova banda em 2004, Velvet Revolver, e apesar do sucesso alcançado, a saída do vocalista Scott Weiland encerrou a carreira do grupo. Em 2010, Slash assinou contrato para uma carreira sob seu próprio nome e lançou seu primeiro disco, Slash, produzido ao lado de vários músicos, e acompanhado por uma banda de apoio que o guitarrista realizou uma turnê mundial entre 2010 e 2011 junto com Myles Kennedy escolhido como vocalista de sua banda. Em 2012, o músico lançou seu novo disco, Apocalyptic Love, novamente com a banda de apoio que lhe ajudou a gravar seu álbum anterior mas que agora contava com a presença fixa do vocalista Myles Kennedy, e iniciou uma nova turnê ao redor do mundo, tour essa que foi muito bem recebida e aclamada pelo público. Em Setembro de 2014, Slash e sua banda Slash featuring Myles Kennedy and The Conspirators lançaram o seu terceiro disco intitulado World On Fire sendo muito bem recebido pela crítica mundial e considerado o disco mais pesado criado pelo guitarrista. No dia 5 de janeiro de 2016, Slash anuncia seu retorno ao Guns N' Roses. Atualmente, Slash é o guitarrista solo do Guns N' Roses e também da sua banda Slash Featuring. Myles Kennedy And The Conspirators.",
    material: [
      {
        name: "Dunlop SW95 Slash Signature Cry Baby Wha",
        text: "",
        image: "Dunlop SW95 Slash Signature Cry Baby Wha.jpg",
      },
      {
        name: "Gibson Les Paul Standard 50's Gold",
        text: "",
        image: "Gibson Les Paul Standard 50's Gold.jpg",
      },
      {
        name: "Marshall JTM45, 1960AX, 1960BX Tube Stack",
        text: "",
        image: "Marshall JTM45, 1960AX, 1960BX Tube Stack.jpg",
      },
    ],
    band: [
      {
        name: "Guns N' Roses",
        date: "",
        image: "Guns N' Roses.jpg",
      },
      {
        name: "Velvet Revolver",
        date: "",
        image: "Velvet Revolver.png",
      },
      {
        name: "Slash's Snakepi",
        date: "",
        image: "../no-image.jpg",
      },
    ],
    highlight: false,
    views: 0,
    page: "artista_slash.html",
  },
  {
    // 7: Theo Katzman
    name: "Theo Katzman",
    role: [1, 2, 5],
    image: "Theo Katzman.jpg",
    biography:
      "Theo Katzman  (nascido em 2 de abril de 1986)  é um multi-instrumentista, cantor, compositor e produtor americano, que vive em Los Angeles, Califórnia . O seu estilo musical é uma fusão de pop, jazz, funk e indie rock. Ele é membro da banda Vulfpeck e contribuiu para trabalhos de vários artistas como compositor e produtor. Katzman lançou três álbuns de estúdio. O seu último álbum, Modern Johnny Sings: Songs in The Age of Vibe, foi lançado em janeiro de 2020.",
    material: [
      {
        name: "Boss DM-2 Analog Delay",
        text: "",
        image: "Boss DM-2 Analog Delay.jpg",
      },
      {
        name: "Gibson ES-335 Electric Guitar",
        text: "",
        image: "Gibson ES-335 Electric Guitar.png",
      },
      {
        name: "Walrus Audio Julia Analog Chorus&Vibrato",
        text: "",
        image: "Walrus Audio Julia Analog Chorus&Vibrato.jpg",
      },
    ],
    band: [
      {
        name: "Vulfpeck",
        date: "",
        image: "Vulfpeck.jpg",
      },
    ],
    highlight: false,
    views: 0,
    page: "artista_theokatzman.html",
  },
];

/* Array de Categorias de Artista
 *
 * name: Nome da Categoria
 * page: Página da Categoria
 */
let artist_role = [
  { name: "Baixistas", page: "baixistas.html" }, // 0: Baixistas
  { name: "Bateristas", page: "bateristas.html" }, // 1: Bateristas
  { name: "Cantores", page: "cantores.html" }, // 2: Cantores
  { name: "Compositores", page: "compositores.html" }, // 3: Compositores
  { name: "DJs", page: "djs.html" }, // 4: DJs
  { name: "Guitarristas", page: "guitarristas.html" }, // 5: Guitarristas
  { name: "Produtores de Música", page: "produtores.html" }, // 6: Produtores de Música
  { name: "Rappers", page: "rappers.html" }, // 7: Rappers
  { name: "Tecladistas", page: "tecladistas.html" }, // 8: Tecladistas
  { name: "Pianistas", page: "pianistas.html" }, // 9: Pianistas
  { name: "Trompetistas", page: "trompetistas.html" }, // 10: Trompetistas
];

let user = [
  {
    name: "Pedro Mendonça",
    email: "pedromendonca@artistbase.com",
    password: "12345",
    birthday: "",
    gender: "",
    image: "pedromendonca.jpg",
    role: [9, 10],
    location: "Porto, Portugal",
    biography: "Clique em editar para inserir a sua biografia.",
    material: [],
    band: [],
    admin: true,
    views: 0,
    page: "utilizador_pedromendonca.html",
  },
  {
    name: "Luís Bilhoto",
    email: "luisbilhoto@artistbase.com",
    password: "12345",
    birthday: "",
    gender: "",
    image: "luisbilhoto.jpg",
    role: [0, 1],
    location: "Lisboa, Portugal",
    biography: "Clique em editar para inserir a sua biografia.",
    material: [],
    band: [],
    admin: false,
    views: 0,
    page: "utilizador_luisbilhoto.html",
  },
];
