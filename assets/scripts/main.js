function placeRoles(element, before = "", after = "") {
  let placeRoles = "";
  if (element === "aside-filters") {
    for (let i = 0; i < artist_role.length; i++) {
      placeRoles +=
        before +
        '<input type="checkbox" name="roles" id="role' +
        i +
        '" checked oninput=\'{placeSearch("search-result", document.getElementById("search").value)}\' /> ' +
        artist_role[i].name +
        "<br>" +
        after;
    }
  } else {
    for (let i = 0; i < artist_role.length; i++) {
      placeRoles +=
        before +
        '<a href="artistas_' +
        artist_role[i].page +
        '">' +
        artist_role[i].name +
        "</a>" +
        after;
    }
  }
  if (element === "dropdown-menu") {
    placeRoles += '<hr><a href="artistas.html">Todas</a>';
  }
  document.getElementById(element).innerHTML = placeRoles;
}

function placeArtists(element, title, role = -1, max = 5) {
  let placeArtists = title,
    count = 0;
  if (role === -1) {
    for (let i = 0; i < artist.length; i++) {
      if (artist[i].highlight && count < max) {
        placeArtists +=
          '<div class="artist-block text-center"><a href="' +
          artist[i].page +
          '"><img src="assets/images/artists/' +
          artist[i].image +
          '" alt="' +
          artist[i].name +
          '">' +
          artist[i].name +
          "</a></div>";
        count++;
      }
    }
  } else {
    for (let i = 0; i < artist.length; i++) {
      for (let j = 0; j < artist[i].role.length; j++) {
        if (artist[i].role[j] === role && count < max) {
          placeArtists +=
            '<div class="artist-block text-center"><a href="' +
            artist[i].page +
            '"><img src="assets/images/artists/' +
            artist[i].image +
            '" alt="' +
            artist[i].name +
            '">' +
            artist[i].name +
            "</a></div>";
          count++;
        }
      }
    }
  }
  document.getElementById(element).innerHTML = placeArtists;
}

// TODO: Este código é extremamente ineficiente! .. Reconstrução necessária..
function placeSearch(
  element,
  string,
  order_name = false,
  order_highlight = false,
  order_views = false
) {
  let placeSearch = "",
    filter_roles = document.getElementsByName("roles"),
    last_id = -1;
  for (let id = 0; id < artist.length; id++) {
    if (artist[id].name.toLowerCase().match(string.toLowerCase())) {
      for (let role = 0; role < artist[id].role.length; role++) {
        for (let filter = 0; filter < filter_roles.length; filter++) {
          if (
            filter_roles[filter].checked &&
            artist[id].role[role] === filter &&
            id !== last_id
          ) {
            placeSearch +=
              '<div class="artist-block text-center"><a href="' +
              artist[id].page +
              '"><img src="assets/images/artists/' +
              artist[id].image +
              '" alt="' +
              artist[id].name +
              '">' +
              artist[id].name +
              "</a></div>";
            last_id = id; // Hotfix: Evitar repetição
          }
        }
      }
    }
  }
  document.getElementById(element).innerHTML = placeSearch;
}

function placeAdminSearch(element, string) {
  let placeAdminSearch = "";
  for (let id = 0; id < artist.length; id++) {
    if (artist[id].name.toLowerCase().match(string.toLowerCase())) {
      placeAdminSearch +=
        "<tr>" +
        '<td class="image">' +
        '<img src="/assets/images/artists/' +
        artist[id].image +
        '" alt="' +
        artist[id].name +
        '" />' +
        "</td>" +
        "<td>" +
        '<a class="font-bold" href="' +
        artist[id].page +
        '">' +
        artist[id].name +
        "</a>" +
        "</td>" +
        '<td class="text-right">' +
        '<a href="">' +
        '<button class="bg-google">Remover</button>' +
        "</a>" +
        "</td>" +
        "</tr>";
    }
  }
  document.getElementById(element).innerHTML = placeAdminSearch;
}

function placeAdminSearchUser(element, string) {
  let placeAdminSearchUser = "";
  for (let id = 0; id < user.length; id++) {
    if (user[id].name.toLowerCase().match(string.toLowerCase())) {
      placeAdminSearchUser +=
        "<tr>" +
        '<td class="image">' +
        '<img src="/assets/images/users/' +
        user[id].image +
        '" alt="' +
        user[id].name +
        '" />' +
        "</td>" +
        "<td>" +
        '<a class="font-bold" href="' +
        user[id].page +
        '">' +
        user[id].name +
        "</a>" +
        "</td>" +
        '<td class="text-right">' +
        '<a href="">' +
        '<button class="bg-green">Validar</button>' +
        '</a> <a href="">' +
        '<button class="bg-google">Banir</button>' +
        "</a>" +
        "</td>" +
        "</tr>";
    }
  }
  document.getElementById(element).innerHTML = placeAdminSearchUser;
}

function checkLogin() {
  let email = document.getElementById("email").value,
    password = document.getElementById("password").value;
  document.getElementById("login-error").innerHTML = "";
  if (!doLogin(email, password)) {
    document.getElementById("login-error").innerHTML =
      "Não foi possível iniciar sessão!<br>O endereço email e/ou a password encontram-se incorrectos.";
  } else {
    window.location.replace("/");
  }
}

function doLogin(email, password) {
  for (let i = 0; i < user.length; i++) {
    if (user[i].email === email && user[i].password === password) {
      createCookie("userid", i, 7);
      return true;
    }
  }
  return false;
}

function updateLogin() {
  let userid = readCookie("userid");
  if (userid !== null) {
    let updateLogin =
      '<ul><li><a href="#"><i id="dropdown-ntoggle" class="fa fa-bell fa-lg"></i></a><div id="dropdown-nmenu" class="text-left"><h4>Notificações</h4><hr>' +
      '<a href="/sugestao3.html"><i class="green fa fa-check" aria-hidden="true"></i>&Tab;Sugestão #3.</a>' +
      '<a href="/sugestao4.html"><i class="green fa fa-check" aria-hidden="true"></i>&Tab;Sugestão #4.</a>' +
      '<a href="/sugestao5.html"><i class="red fa fa-times" aria-hidden="true"></i>&Tab;Sugestão #5.</a>' +
      '<a href="/sugestao6.html"><i class="red fa fa-times" aria-hidden="true"></i>&Tab;Sugestão #6.</a>' +
      "</div></li>" +
      '<li><a href="#" id="dropdown-utoggle"><img id="dropdown-utoggle-img" src="assets/images/users/' +
      user[userid].image +
      '" alt="' +
      user[userid].name +
      '" /> ' +
      user[userid].name +
      " &blacktriangledown;</a>" +
      '<div id="dropdown-umenu" class="text-left">';
    if (user[userid].admin) {
      updateLogin +=
        '<a href="/admin.html"><i class="fa fa-user" aria-hidden="true"></i> Administração</a>';
    } else {
      updateLogin +=
        '<a href="/utilizador.html"><i class="fa fa-user" aria-hidden="true"></i> Minha Conta</a>' +
        '<a href="/sugestoes.html"><i class="fa fa-book" aria-hidden="true"></i> Sugestões</a>';
    }
    updateLogin +=
      '<a href="/logout.html"><i class="fa fa-sign-out" aria-hidden="true"></i> Sair</a>' +
      "</div></li></ul>";
    document.getElementById("navbar-user").innerHTML = updateLogin;
  }
}

function displayID(element, toogle, element_hide = [], toogle_hide = []) {
  document.getElementById(element).style.display = "block";
  for (let i = 0; i < element_hide.length; i++) {
    document.getElementById(element_hide[i]).style.display = "none";
  }
  document.getElementById(toogle).className = "active";
  for (let i = 0; i < toogle_hide.length; i++) {
    document.getElementById(toogle_hide[i]).className = "";
  }
}

function placeArtistsPage(element, id) {
  let placeArtistsPage =
    '<img class="artist-picture" src="assets/images/artists/' +
    artist[id].image +
    '" alt="' +
    artist[id].name +
    '" />' +
    "<h3>" +
    artist[id].name +
    '</h3><h5 class="font-normal">';
  for (let rid = 0; rid < artist[id].role.length; rid++) {
    if (rid > 0) {
      placeArtistsPage += ", ";
    }
    placeArtistsPage += artist_role[artist[id].role[rid]].name;
  }
  placeArtistsPage +=
    "</h5><hr />" +
    '<div class="btn-group">' +
    "<a href=\"#biography\" onclick=\"displayID('biography', 'btn-biography', ['material', 'bands'], ['btn-material', 'btn-bands'])\">" +
    '<button id="btn-biography" class="active">Biografia</button></a>' +
    "<a href=\"#material\" onclick=\"displayID('material', 'btn-material', ['biography', 'bands'], ['btn-biography', 'btn-bands'])\">" +
    '<button id="btn-material">Material</button></a>' +
    "<a href=\"#bands\" onclick=\"displayID('bands', 'btn-bands', ['biography', 'material'], ['btn-biography', 'btn-material'])\">" +
    '<button id="btn-bands">Bandas</button></a>' +
    "</div>" +
    '<div id="biography"><p id="biography-text">' +
    artist[id].biography +
    "</p>";
  if (readCookie("userid") !== null) {
    placeArtistsPage +=
      '<div class="edit text-right"><a><button id="btn-editbio" onclick="editBiography()">Editar</button></a></div>';
  }
  placeArtistsPage += '</div><div id="material"><table><tr>';
  for (let mid = 0; mid < artist[id].material.length; mid++) {
    placeArtistsPage +=
      '<td><img src="assets/images/materials/' +
      artist[id].material[mid].image +
      '" alt="' +
      artist[id].material[mid].name +
      '" /><p>' +
      artist[id].material[mid].name +
      "</p></td>";
  }
  placeArtistsPage += "</tr></table>";
  if (readCookie("userid") !== null) {
    placeArtistsPage +=
      '<div class="edit text-right"><a href="editar_material.html?return=' +
      artist[id].page +
      '"><button>Editar</button></a></div>';
  }
  placeArtistsPage += '</div><div id="bands"><table><tr>';
  for (let bid = 0; bid < artist[id].band.length; bid++) {
    placeArtistsPage +=
      '<td><img src="assets/images/bands/' +
      artist[id].band[bid].image +
      '" alt="' +
      artist[id].band[bid].name +
      '" /><p>' +
      artist[id].band[bid].name +
      "</p></td>";
  }
  placeArtistsPage += "</tr></table>";
  if (readCookie("userid") !== null) {
    placeArtistsPage +=
      '<div class="edit text-right"><a href="editar_bandas.html?return=' +
      artist[id].page +
      '"><button>Editar</button></a></div>';
  }
  placeArtistsPage += "</div>";
  document.getElementById(element).innerHTML = placeArtistsPage;
}

// Yh está bem feito... TODO: Melhorar isto..
function placeArtistsAside(element, id) {
  let placeArtistsAside =
    '<h3>RELACIONADOS</h3><hr /><table class="text-left">';
  last_id = [];
  for (let u_rid = 0; u_rid < artist[id].role.length; u_rid++) {
    for (let other = 0; other < artist.length; other++) {
      for (let o_rid = 0; o_rid < artist[other].role.length; o_rid++) {
        if (
          artist[id].role[u_rid] === artist[other].role[o_rid] &&
          other !== id &&
          !last_id.includes(other)
        ) {
          placeArtistsAside +=
            '<tr><td class="col-img">' +
            '<a href="' +
            artist[other].page +
            '"><img src="assets/images/artists/' +
            artist[other].image +
            '" alt="' +
            artist[other].name +
            '" /></a>' +
            "</td><td>" +
            '<a href="' +
            artist[other].page +
            '">' +
            "<h4>" +
            artist[other].name +
            '</h4><h5 class="font-normal">' +
            artist_role[artist[other].role[o_rid]].name +
            "</h5>" +
            "</a></td></tr>";
          last_id[last_id.length] = other;
        }
      }
    }
  }
  placeArtistsAside +=
    '</table><div class="text-left">' +
    '<a href="artistas_' +
    artist_role[artist[id].role[0]].page +
    '">+ Ver Relacionados</a></div>';
  document.getElementById(element).innerHTML = placeArtistsAside;
}

function editBiography() {
  document.getElementById("biography").innerHTML =
    '<textarea id="biography-text" name="biography" rows="15">' +
    document.getElementById("biography-text").textContent +
    "</textarea>" +
    '<div class="edit text-right"><a><button id="btn-submitbio" onclick="submitBiography()">Sugerir</button></a></div>';
}

function submitBiography() {
  document.getElementById("biography").innerHTML =
    '<div class="bg-green">A sua sugestão foi submetida com sucesso<br><a href="/sugestoes.html">Visite a página de Sugestões para acompanhar o seguimento da sua sugestão</a></div><br><p id="biography-text">' +
    document.getElementById("biography-text").value +
    '</p><div class="edit text-right"><a><button id="btn-editbio" onclick="editBiography()">Editar</button></a></div>';
}

function placeUserPage(element, id = -1) {
  let sid = readCookie("userid"),
    placeUserPage = "";
  if (id === -1 && sid !== null) {
    id = sid;
  }
  if (id !== -1) {
    if (!user[id].admin && sid === id) {
      placeUserPage +=
        '<a class="btn-topright" href="validacao.html?return=utilizador.html"><button class="bg-green">Tornar-me Artista</button></a>';
    }
    placeUserPage +=
      '<img class="artist-picture" src="assets/images/users/' +
      user[id].image +
      '" alt="' +
      user[id].name +
      '" />' +
      "<h3>" +
      user[id].name +
      '</h3><h5 class="font-normal">';
    for (let rid = 0; rid < user[id].role.length; rid++) {
      if (rid > 0) {
        placeUserPage += ", ";
      }
      placeUserPage += artist_role[user[id].role[rid]].name;
    }
    placeUserPage +=
      "</h5><h6>" +
      user[id].location +
      "</h6><hr />" +
      '<div class="btn-group">' +
      "<a href=\"#biography\" onclick=\"displayID('biography', 'btn-biography', ['material', 'bands'], ['btn-material', 'btn-bands'])\">" +
      '<button id="btn-biography" class="active">Biografia</button></a>' +
      "<a href=\"#material\" onclick=\"displayID('material', 'btn-material', ['biography', 'bands'], ['btn-biography', 'btn-bands'])\">" +
      '<button id="btn-material">Material</button></a>' +
      "<a href=\"#bands\" onclick=\"displayID('bands', 'btn-bands', ['biography', 'material'], ['btn-biography', 'btn-material'])\">" +
      '<button id="btn-bands">Bandas</button></a>' +
      "</div>" +
      '<div id="biography"><p id="biography-text">' +
      user[id].biography +
      "</p>";
    if (sid === id) {
      placeUserPage +=
        '<div class="edit text-right"><a><button id="btn-editbio" onclick="editBiographyUser()">Editar</button></a></div>';
    }
    placeUserPage += '</div><div id="material"><table><tr>';
    for (let mid = 0; mid < user[id].material.length; mid++) {
      placeUserPage +=
        '<td><img src="assets/images/materials/' +
        user[id].material[mid].image +
        '" alt="' +
        user[id].material[mid].name +
        '" /><p>' +
        user[id].material[mid].name +
        "</p></td>";
    }
    placeUserPage += "</tr></table>";
    if (sid === id) {
      placeUserPage +=
        '<div class="edit text-right"><a href="editar_material.html?return=utilizador.html"><button>Editar</button></a></div>';
    }
    placeUserPage += '</div><div id="bands"><table><tr>';
    for (let bid = 0; bid < user[id].band.length; bid++) {
      placeUserPage +=
        '<td><img src="assets/images/bands/' +
        user[id].band[bid].image +
        '" alt="' +
        user[id].band[bid].name +
        '" /><p>' +
        user[id].band[bid].name +
        "</p></td>";
    }
    placeUserPage += "</tr></table>";
    if (sid === id) {
      placeUserPage +=
        '<div class="edit text-right"><a href="editar_bandas.html?return=utilizador.html"><button>Editar</button></a></div>';
    }
    placeUserPage += "</div>";
  } else {
    placeUserPage =
      '<div id="login-error" class="bg-google">Erro ao carregar o utilizador!</div>';
    window.location.replace("/login.html");
  }
  document.getElementById(element).innerHTML = placeUserPage;
}

function editBiographyUser() {
  document.getElementById("biography").innerHTML =
    '<textarea id="biography-text" name="biography" rows="15">' +
    document.getElementById("biography-text").textContent +
    "</textarea>" +
    '<div class="edit text-right"><a><button id="btn-submitbio" onclick="submitBiographyUser()">Submeter</button></a></div>';
}

function submitBiographyUser() {
  document.getElementById("biography").innerHTML =
    '<p id="biography-text">' +
    document.getElementById("biography-text").value +
    '</p><div class="edit text-right"><a><button id="btn-editbio" onclick="editBiographyUser()">Editar</button></a></div>';
}

function updateReturn(element) {
  let updateReturn = new URLSearchParams(window.location.search).get("return");
  if (updateReturn === null) {
    updateReturn = "";
  }
  if (!updateReturn.match("utilizador.html")) {
    document.getElementById(element).innerHTML =
      '<a href="' +
      updateReturn +
      '"><button class="button-submit">SUGERIR</button></a>';
  } else {
    document.getElementById(element).innerHTML =
      '<a href="' +
      updateReturn +
      '"><button class="button-submit">SUBMETER</button></a>';
  }
}

//////////////////////////////////////////////////
// Source: https://www.quirksmode.org/js/cookies.html
//////////////////////////////////////////////////
function createCookie(name, value, days) {
  let expires = "";
  if (days) {
    let date = new Date();
    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
    expires = "; expires=" + date.toGMTString();
  }
  document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
  let nameEQ = name + "=";
  let ca = document.cookie.split(";");
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == " ") c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
  }
  return null;
}

function eraseCookie(name) {
  createCookie(name, "", -1);
}
//////////////////////////////////////////////////

window.onclick = function (e) {
  // Menu Dropdown (Artistas)
  let dropdown_menu = document.getElementById("dropdown-menu");
  if (e.target.matches("#dropdown-toggle")) {
    if (window.getComputedStyle(dropdown_menu).display === "block") {
      dropdown_menu.style.display = "none";
    } else {
      dropdown_menu.style.display = "block";
    }
  } else {
    dropdown_menu.style.display = "none";
  }

  // Menu Dropdown (Notificações e Utilizador)
  if (readCookie("userid") !== null) {
    let dropdown_nmenu = document.getElementById("dropdown-nmenu");
    if (e.target.matches("#dropdown-ntoggle")) {
      if (window.getComputedStyle(dropdown_nmenu).display === "block") {
        dropdown_nmenu.style.display = "none";
      } else {
        dropdown_nmenu.style.display = "block";
      }
    } else {
      dropdown_nmenu.style.display = "none";
    }

    let dropdown_umenu = document.getElementById("dropdown-umenu");
    if (
      e.target.matches("#dropdown-utoggle") ||
      e.target.matches("#dropdown-utoggle-img")
    ) {
      if (window.getComputedStyle(dropdown_umenu).display === "block") {
        dropdown_umenu.style.display = "none";
      } else {
        dropdown_umenu.style.display = "block";
      }
    } else {
      dropdown_umenu.style.display = "none";
    }
  }
};
